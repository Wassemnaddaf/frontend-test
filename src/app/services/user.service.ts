import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const baseUrl = 'https://reqres.in/api/users';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  get(page): Observable<any> {
    return this.http.get(`${baseUrl}?page=${page}`);
  }

  getSingle(id): Observable<any> {
    return this.http.get(`${baseUrl}/${id}`);
  }
}
