import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { TableOperations } from 'src/app/shared/TableOperations';
import {NzNotificationService} from 'ng-zorro-antd';
import { fadeInAnimation } from 'src/app/animations';
@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
  animations: [fadeInAnimation],
  host: { '[@fadeInAnimation]': '' }
})
export class UsersListComponent extends TableOperations implements OnInit {

  constructor(private userService: UserService,private _notification: NzNotificationService) { 
    super(userService, true, _notification); // pass user service , if the data is paginated or not
  }

  ngOnInit(): void {
    this.fetchItems()
  }
  

}
