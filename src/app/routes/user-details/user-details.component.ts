import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { fadeInAnimation } from 'src/app/animations';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss'],
  animations: [fadeInAnimation],
  host: { '[@fadeInAnimation]': '' }
})
export class UserDetailsComponent implements OnInit {

  user: any = {};
  loading: boolean = false;

  constructor(private userService: UserService, private route: ActivatedRoute) {
    this.user.id = this.route.snapshot.params.id;
  }

  ngOnInit(): void {
    this.fetchUser();
  }

  fetchUser() {
    this.loading = true;
    this.userService.getSingle(this.user.id).subscribe(
      data => { this.user = data.data; this.loading = false; },
      error => { console.log(error); this.loading = false; }
    );
  }

}
