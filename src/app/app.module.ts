import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { UsersListComponent } from './routes/users-list/users-list.component';
import { UserDetailsComponent } from './routes/user-details/user-details.component';
import { HeaderComponent } from './components/header/header.component';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzAvatarModule, NzDescriptionsModule, NzIconModule, NzInputModule, NzNotificationModule, NzPageHeaderModule, NzPaginationModule, NzSkeletonModule, NzSpinModule } from 'ng-zorro-antd';
import { UserCardComponent } from './components/user-card/user-card.component';
import { StorageRequestService } from './services/storage-request.service';
import { CachingInterceptor } from './interceptors/cache.interceptor';

registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    UsersListComponent,
    UserDetailsComponent,
    HeaderComponent,
    UserCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    NzGridModule,
    NzCardModule,
    NzLayoutModule,
    NzAvatarModule,
    NzSkeletonModule,
    NzPageHeaderModule,
    NzInputModule,
    NzIconModule,
    NzPaginationModule,
    NzSpinModule,
    NzDescriptionsModule,
    NzNotificationModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US },
    StorageRequestService,
    { provide: HTTP_INTERCEPTORS, useClass: CachingInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
