import { OnInit, Optional } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd';
import { Pagination } from '../models/Pagination';

export class TableOperations {

    items = []; // table items
    service: any; // component's serviec to make HTTP requests
    loading: boolean = false; // loader flag when there are pending network requests
    isPagination: boolean = false; // to detect if current table is paginated or not
    pagination = new Pagination(); // pagination meta data (page, total, ..etc).
    searchFilter: string = '';
    constructor(service: any, isPagination: boolean, @Optional() private notification = null) {
        this.service = service;
        this.isPagination = isPagination;
    }

    fetchItems() {
        this.loading = true; // play loader

        this.service.get(this.pagination.curPage)
            .subscribe(
                data => { this.handleItems(data) },
                error => { this.handleError(error) }
            );
    }
    handleItems(data: any) {
        // check if data is paginated to get pagination data
        if (this.isPagination) {
            this.items = data.data;
            this.pagination.total = data.total;
            this.pagination.perPage = data.per_page;
            this.pagination.totalPages = data.total_pages;
        }
        else this.items = data;

        this.loading = false; // stop loader
    }

    handleError(error) {
        console.log(error) // notify error
        this.notification.create(
            'error',
            'Error',
            error.message
        );
        this.loading = false; // stop loader
    }

    handleSearch(value) {
        if (value === '') {
            this.fetchItems();
            return;
        }
        this.loading = true;
        this.service.getSingle(value)
            .subscribe(
                data => { this.items = [data.data]; this.loading = false },
                error => { this.items = []; this.handleError(error) }
            );
    }

    handlePaginate(page) {
        this.pagination.curPage = page;
        this.fetchItems()
    }


}