import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss']
})
export class UserCardComponent implements OnInit {

  @Input() user: any;
  @Input() loading = true;

  constructor(private router: Router) { }

  ngOnInit(): void {}

  showProfile() {
    this.router.navigate([`users/${this.user.id}`])
  }
}
