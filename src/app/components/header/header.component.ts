import { Location } from '@angular/common';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @Input() title: string;
  @Input() subtitle: string = null;

  constructor(private location: Location,private activeRoute: ActivatedRoute) { }

  ngOnInit(){
  }

  onBack() {
  }

  isHome() {
    return this.activeRoute.snapshot.routeConfig.path === 'users'
  }
}
